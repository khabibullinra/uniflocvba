'расчет забойного давления через давление на приеме насоса
'выполняется по корреляции Беггса Брилла
Public Function tr_BB_Pwf_Pin_atma(Dcas_mm, Hvd_m, Hpump_m, _
        Udl_Hvd_m, Pin_atmg, Qliq_sm3day, wc_perc, rp_m3t, _
        gamma_oil, Pb_atma, bo_m3m3, Optional Tres_C = 60, _
        Optional dtub_mm = 72, Optional gamma_gas = 0.8, _
        Optional Udl_Hpump_m = 0)
' Dcas_mm       - внутренний диаметр эксплуатационной коллоны, мм
' Hvd_m         - глубина верхних дыр перфорации, измеренная, м
' Hpump_m       - глубина спуска насоса, измеренная, м
' Udl_Hvd_m     - удлинение на глубину верхних дыр перфорации, м
' Pin_atmg      - давление на приеме, атм избыточное
' Qliq_sm3day   - дебит жидкости в стандартных условиях, м3/сут
' wc_perc       - обводненность в стандартных условиях, %
' rp_m3t        - газовый фактор, м3/т
' gamma_oil     - плотность нефти в стандартных условиях, г/см3
' Pb_atma       - давление насыщения, атм абсолютное
' bo_m3m3       - объемный коэффициент нефти, м3/м3
' Tres_C = 60   - температура пластовая, C
' Dtub_mm = 72  - внешний диаметр НКТ, мм
' GammaGas=0.8  - плотность газа
' Udl_Hpump_m   - удлинение на глубину спуска насоса