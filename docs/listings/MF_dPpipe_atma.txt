'  расчет перепада давления и распределения температуры в трубе
'  с использованием многофазных корреляций
Public Function MF_dPpipe_atma(Q_m3Day, WCT_perc, _
                Hmes0_m As Double, Hmes1_m As Double, _
                Pcalc_atma As Double, _
                Optional Tcalc_C As Double = 50, _
                Optional theta_deg As Double = 90, _
                Optional d_mm As Double = 60, _
                Optional roughness_m As Double = 0.0001, _
                Optional gamma_gas = const_gamma_gas_default, _
                Optional gamma_oil = const_gamma_oil_default, _
                Optional gamma_wat = const_gamma_wat_default, _
                Optional Rsb_m3m3 = const_Rsb_default, _
                Optional Rp_m3m3 As Double = -1, _
                Optional Pb_atma As Double = -1, _
                Optional Tres_C As Double = const_Tres_default, _
                Optional Bob_m3m3 As Double = -1, _
                Optional Muob_cP As Double = -1, _
                Optional PVTcorr = StandingBased, _
                Optional Ksep_fr As Double = 0, _
                Optional PKsep_atma As Double = -1, _
                Optional TKsep_C As Double = -1, _
                Optional HydrCorr As H_CORRELATION = 0, _
                Optional Calc_from_H0 As Boolean = True, _
                Optional Tcalc_method As Integer = 1, _
                Optional Tother_C As Double = -1, _
                Optional Tgrad_c100m As Double = 3)
'
' Обязательные параметры
' Q_m3Day       - дебит жидкости в поверхностных условиях
' WCT_perc      - обводненность
' Hmes0_m       - начальная координата трубы, м
' Hmes1_m       - конечная координата трубы, м
' Pcalc_atma    - давление с которого начинается расчет, атм
'                 граничное значение для проведения расчета
' Необязательные параметры
' стандартные набор PVT параметров
' Tcalc_C       - температура в точке где задано давление, С
' theta_deg     - угол направления потока к горизонтали
'                 (90 - вертикальная труба вверх)
'                 может принимать отрицательные значения
' d_mm          - внутрнний диаметр трубы
' roughness_m   - шероховатость трубы
' gamma_gas     - удельная плотность газа, по воздуху.
'                   const_gamma_gas_default = 0.6
' gamma_oil     - удельная плотность нефти, по воде.
'                   const_gamma_oil_default = 0.86
' gamma_wat     - удельная плоность воды, по воде.
'                   const_gamma_wat_default = 1
' Rsb_m3m3      - газосодержание при давлении насыщения, м3/м3.
'                   const_Rsb_default = 100
' Rp_m3m3       - замерной газовый фактор, м3/м3.
' Pb_atma       - давление насыщения, атм. Калибровочный параметр.
' Tres_C        - пластовая температура, С.
'                 Учитывается при расчете давления насыщения.
' Bob_m3m3      - объемный коэффициент нефти, м3/м3.
' Muob_cP       - вязкость нефти при давлении насыщения
'                 По умолчанию рассчитывается по корреляции
' PVTcorr       - номер набора PVT корреляций для расчета
'                 StandingBased = 0 - на основе кор-ии Стендинга
'                 McCainBased = 1 - на основе кор-ии Маккейна
'                 StraigthLine = 2 - упрощенные зависимости
' Ksep_fr       - коэффициент сепарации - определяет изменение
'               свойств нефти после сепарации  доли  газа.
'               изменение свойств нефти зависит от условий
'               сепарация газа, которые должны быть явно заданы
' PKsep_atma    - давление при которой была сепарация
' TKsep_C       - температура при которой была сепарация
' HydrCorr      - гидравлическая корреляция, H_CORRELATION
'                   BeggsBriilCor = 0
'                   AnsariCor = 1
'                   UnifiedCor = 2
'                   Gray = 3
'                   HagedornBrown = 4
'                   SakharovMokhov = 5
' Calc_from_H0  - флаг показывает направление расчета
'                   1 (True) расчет из H0 в H1
'                   0 (False) расчет из H1 в H0
'                 переключение "снизу вверх" и "сверху вниз"
' Tcalc_method  - переключение метода расчета температуры
'                   0 - расчет по температуре на концах
'                   1 - по геоградиенту (по умолчанию)
'                   2 - расчет эмисии тепла
' Tother_C      - температура на другом конце трубы (опция 0)
' Tgrad_c100m   - гео градиент температуры C на 100 м верт глубины
'                 (опция 1)
' выходное значение - число - давление на другом конце трубы atma.
