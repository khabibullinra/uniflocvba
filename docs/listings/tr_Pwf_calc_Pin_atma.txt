' функция расчета забойного давления по давлению на приеме, atma
' входные параметры адаптированы для данных тех режима скважин
' возвращает абсолютное значение давления!
Public Function tr_Pwf_calc_Pin_atma(Pin_atmg As Double, _
        Hvd_m As Double, Udl_Hvd_m As Double, Hpump_m As Double, _
        Dcas_mm As Double, Pb_atma As Double, rp_m3t As Double, _
        Qliq_sm3day As Double, _
        Optional bo_m3m3 = 1.2, Optional rho_oil_kgm3 = 860, _
        Optional wc_perc = 0, Optional rho_wat_kgm3 = 1000, _
        Optional Qg_mes_sm3day As Double = -1, _
        Optional tr_HydCor As Integer = 2, _
        Optional Bw_m3m3 As Double = 1, _
        Optional Tres_C As Double = 60, _
        Optional Zav As Double = 0.9, _
        Optional gamma_g As Double = 0.8, _
        Optional Udl_Hpump_m As Double = -1)
' Входные параметры
' Pin_atmg      - давление на приеме насоса, измеренное, атм
' Hvd_m         - глубина верхних дыр перфорации, измеренная, м
' Udl_Hvd_m     - удлинение на глубину верхних дыр перфорации, м
' Hpump_m       - глубина спуска насоса, измеренная, м
' Dcas_mm       - внутренний диаметр эксплуатационной коллоны, мм
' Pb_atma       - давление насыщения, атм абсолютное
' rp_m3t        - газовый фактор, м3/т
' Qliq_sm3day   - дебит жидкости в стандартных условиях, м3/сут
'               опциональные параметры
' bo_m3m3       - объемный коэффициент нефти, м3/м3
' rho_oil_kgm3  - плотность нефти в стандартных условиях, г/см3
' wc_perc       - обводненность в стандартных условиях, %
' rho_wat_kgm3  - плотность воды, кг/м3
' Qg_mes_sm3day - замер расхода газа в затрубе,
'               например по отжиму. если < 0 игнорируется
' tr_HydCor   - корреляция для расчета градиента давления
' Dtub_mm = 60  - внешний диаметр НКТ, мм
' Bw_m3m3 = 1   - объемный коэффициент для воды
' Tres_C = 60   - температура пластовая, C
' Zav = 0.9     - сверхсжимаемость газа в затрубе
' GammaGas=0.8  - плотность газа
' Udl_Hpump_m   - удлинение на глубину спуска насоса,м

