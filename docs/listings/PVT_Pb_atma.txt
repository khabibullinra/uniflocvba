' Расчет давления насыщения
Public Function PVT_Pb_atma(T_C, _
                    Optional gamma_gas = const_gamma_gas_default, _
                    Optional gamma_oil = const_gamma_oil_default, _
                    Optional gamma_wat = const_gamma_wat_default, _
                    Optional Rsb_m3m3 = const_Rsb_default, _
                    Optional Rp_m3m3 As Double = -1, _
                    Optional Pbcal_atma As Double = -1, _
                    Optional Tres_C As Double = const_Tres_default, _
                    Optional Bob_m3m3 As Double = -1, _
                    Optional Muob_cP As Double = -1, _
                    Optional PVTcorr = StandingBased, _
                    Optional Ksep_fr As Double = 0, _
                    Optional PKsep_atma As Double = -1, _
                    Optional TKsep_C As Double = -1 _
                    ) As Double
' Т_C       температура, С.
' опциональные аргументы фукнции
' gamma_gas        удельная плотность газа, по воздуху.
'           const_gamma_gas_default = 0.6
' gamma_oil        удельная плотность нефти, по воде.
'           const_gamma_oil_default = 0.86
' gamma_wat        удельная плоность воды, по воде.
'           const_gamma_wat_default = 1
' Rsb_m3m3  газосодержание при давлении насыщения, м3/м3.
'           const_Rsb_default = 100
' Rp_m3m3   замерной газовый фактор, м3/м3.
' Pb_atma   давление насыщения, атм. Калибровочный параметр.
' Tres_C    пластовая температура, С.
'           Учитывается при расчете давления насыщения.
'           const_Tres_default = 90
' Bob_m3m3  объемный коэффициент нефти, м3/м3.
' Muob_cP   вязкость нефти при давлении насыщения
'           По умолчанию рассчитывается по корреляции
' PVTcorr   номер набора PVT корреляций для расчета
'           StandingBased = 0 - на основе кор-ии Стендинга
'           McCainBased = 1 - на основе кор-ии Маккейна
'           StraigthLine = 2 - на основе упрощенных зависимостей
' Ksep_fr   коэффициент сепарации - определяет изменение свойств
'       нефти после сепарации определенной доли свободного газа.
'       изменение свойств нефти зависит от условий при которых
'       произошла сепарация газа, которые должны быть явно заданы
' PKsep_atma    давление при которой была сепарация
' TKsep_C       температура при которой была сепарация
'
' выходное значение - число - давление насыщения.