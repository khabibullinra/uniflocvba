'=======================================================================================
'Unifloc7.2  Canis Lupus                                          khabibullinra@gmail.com
'���������� ��������� ������� �� ��������� �����������
'2000 - 2018 �
'
'=======================================================================================
'
Option Explicit
' ����� ��� ������� � ���� �������
Private ESPload As CESPpump
Public ESPcollection As New Collection
Private p_SpreadSheetName As String
 ' ���� ������ �������� ������
 'Public' LogMsg As New CLogger                ' ������
Public Function NumPumps() As Integer
    NumPumps = ESPcollection.count
End Function
Public Function GetBestPump(Qmix_m3day As Double) As CESPpump
Dim ESP As CESPpump
Dim maxEff As Double
Dim CurEff As Double
maxEff = 0
 
 For Each ESP In ESPcollection
     CurEff = ESP.Get_ESP_effeciency_fr(Qmix_m3day)
     If CurEff > maxEff Then
        maxEff = CurEff
        Set GetBestPump = ESP
     End If
 Next
 
 If maxEff = 0 Then GetBestPump = Nothing
 
 
End Function
Public Sub ClearDB()
  Dim i As Long
  
  For i = 1 To ESPcollection.count    ' Remove name from the collection.
        ESPcollection.Remove 1    ' Since collections are reindexed
                ' automatically, remove the first
  Next        ' member on each iteration.
End Sub
Public Sub LoadDB()
Dim i As Long
Dim StartCell As Long
Dim EndCell As Long
Dim currID As Integer, currIDnew As Integer
Dim frNom As Double
Dim NumPumps As Integer
NumPumps = 0
Dim rateRange
Dim headRange
Dim powerRange
Dim effRange
Dim Item
Dim Index
Call ClearDB
i = 4   ' ��� ���������� � 4 ������
'With Workbooks("UniflocVBA7.xlam").Worksheets(p_SpreadSheetName)
With ThisWorkbook.Worksheets(p_SpreadSheetName)
Do
    StartCell = i
    currID = .Cells(i, 2)
    Do
        EndCell = i
        i = i + 1
        currIDnew = .Cells(i, 2)
    Loop Until currIDnew <> currID Or currID = 0
    
    If EndCell - StartCell > 3 Then
        rateRange = .Range(.Cells(StartCell, 12), .Cells(EndCell, 12))
        headRange = .Range(.Cells(StartCell, 13), .Cells(EndCell, 13))
        powerRange = .Range(.Cells(StartCell, 14), .Cells(EndCell, 14))
        effRange = .Range(.Cells(StartCell, 15), .Cells(EndCell, 15))
        frNom = .Cells(StartCell, 10)
        
        Set ESPload = New CESPpump
        Call ESPload.LoadESP_points("test", rateRange, headRange, powerRange, effRange, frNom)
        ESPload.ID = currID
        ESPload.StageNum = 1
        ESPload.Frequency = .Cells(StartCell, 10)
        ESPload.ManufacturerName = .Cells(StartCell, 3)
        ESPload.PumpName = .Cells(StartCell, 4)
        ESPload.MaxStagesNumber = .Cells(StartCell, 5)
        ESPload.NominalRate_m3day = .Cells(StartCell, 6)
        ESPload.OptimumMinRate_m3day = .Cells(StartCell, 7)
        ESPload.OptimumMaxRate_m3day = .Cells(StartCell, 8)
        ESPcollection.Add ESPload, CStr(currID)
        NumPumps = NumPumps + 1
    End If
Loop Until currID = 0
End With
End Sub
Private Sub Class_Initialize()
    p_SpreadSheetName = "���� �������"
    Call LoadDB
End Sub
Public Function GetPump(ByVal ID As Integer) As CESPpump
On Error GoTo err1:
    Set GetPump = ESPcollection.Item(CStr(ID))
Exit Function
err1:
    addLogMsg "������ ��� �������� �� ���� ������ ID = " & ID & " ."
    Err.Raise kErrESPbase, , "�� ������� ����� ����� " & ID & " � ����"
End Function
