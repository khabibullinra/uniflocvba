'=======================================================================================
'Unifloc7.2  Canis Lupus                                          khabibullinra@gmail.com
'���������� ��������� ������� �� ��������� �����������
'2000 - 2018 �
'
'=======================================================================================
' ������ � ����� �������
'
'
'
'
Public ESPbase As CESPBase
Public ESP As CESPpump
Public Motor As New CESPMotor
Public MotorBase As New CESPMotorBase
Public Function LoadESPbase() As Boolean
' �������� ������ � ������ ID �� ���� �������
    Dim numESP As Integer
On Error GoTo err1:
    addLogMsg "��������� ���� �������"
    Set ESPbase = New CESPBase
    Call ESPbase.LoadDB
    numESP = ESPbase.NumPumps
    addLogMsg "���� ������� ���������, ����� ������� " & numESP & " �������"
    
'    Set ESP = ESPbase.GetPump(ID) ' ESPcollection.Item(CStr(ID))
    LoadESPbase = True
Exit Function
err1:
    addLogMsg "�� ������� ��������� ���� �������."
    LoadESPbase = False
End Function
Public Function checkID(ID) As Boolean
' �������� ������������ �� ���������� ID ���� ������� �������� �� ��������� ����������
On Error GoTo err1:
    If ESP Is Nothing Then
        If Not LoadESPbase Then checkID = False
        Set ESP = ESPbase.GetPump(ID) ' ESPcollection.Item(CStr(ID))
        checkID = True
    End If
    If ESP.ID = ID Then
        checkID = True
    Else
        Set ESP = ESPbase.GetPump(ID) ' ESPcollection.Item(CStr(ID))
        checkID = True
    End If
Exit Function
err1:
    addLogMsg "������ ��� �������� ������ ID " & ID & " ."
    checkID = False
End Function
Public Function getESP(ID) As CESPpump
    Dim newESP As New CESPpump
    If checkID(ID) Then
        Call newESP.Copy(ESP)
        Set getESP = newESP
    End If
End Function
